# More Hardpoints for the Boars Head 

Adds additional hard points to the Atlas Boar's Head and upgrades it to double heatsinks

## Using this mod in Mechwarrior 5

To use this mod download it from Nexus Mods

## Using these files in your own mod

Feel free to use these files in your own Mechwarrior 5 mods! Please give credit to the author `moddingisthegame`.

1. Purchase [Mechwarrior 5 for PC](https://www.epicgames.com/store/en-US/p/mechwarrior-5)
2. Install the [Mechwarrior 5 Modding Toolkt](https://www.epicgames.com/store/en-US/p/mechwarrior-5--mod-editor)
3. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4. Navigate to the Mechwarrior 5 Editor Plugins directory (typically C:\Program Files\Epic Games\MechWarrior5Editor\MW5Mercs\Plugins) in your terminal of choice
5. `git clone https://gitlab.com/mechwarrior5mods/mechs/as7bhmorehardpoints.git`
6. Launch the Mechwarrior 5 Modding Toolkit.
7. Click the dropdown next to Manage Mod and select `as7bhmorehardpoints`
8. Mod away!

## Compatibility

* This mod overrides the MDA and loadout files for the AS7-BH
